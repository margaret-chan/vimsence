# VimSence

Discord rich presence for Vim



## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

You need to have Vim with Python 3 support

### Installing

#### [Pathogen](https://github.com/tpope/vim-pathogen)

```sh
cd ~/.vim/bundle
git https://gitlab.com/margaret-chan/vimsence.git
```

## Authors

* **Margaret Bartkowski** - *Initial work* - [](https://gitlab.com/margaret-chan/)

See also the list of [contributors](https://gitlab.com/margaret-chan/vimsence/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
