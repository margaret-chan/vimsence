import vim
import rpc
import time
import logging

start_time = int(time.time())
base_activity = {
        'details': 'Doing nothing productive',
        'timestamps': {
            "start": start_time
        },
        'assets': {
            'small_text': 'Vim',
            'small_image': 'vim_logo',
            'large_text': 'Vim',
            'large_image': 'vim_logo'
        }
    }

client_id = '519855736247943180'

try:
    rpc_obj = rpc.DiscordIpcClient.for_platform(client_id)
    rpc_obj.set_activity(base_activity)
except Exception as e:

    pass

def update_presence():

    activity = base_activity
    file_type = evaluate_extension(get_extension())
    activity['details'] = file_type + ' file: ' + get_filename()
    if get_extension():
        activity['assets']['large_image'] = get_extension()

    try:
        rpc_obj.set_activity(activity)
    except BrokenPipeError as e:
        pass
    except NameError as e:
        
        pass

def get_filename():
    """Get current filename that is being edited
    :returns: string
    """
    return vim.eval('expand("%:t")')
def evaluate_extension(extension):
    if extension == 'pl':
        return 'Perl'
    elif extension == 'py':
        return 'Python'
    elif extension == 'c':
        return 'C'
    elif extension == 'go':
        return 'Golang'
    elif extension == 'md':
        return 'Markdown'
    elif extension == 'cpp':
        return 'C++'
    elif extension == 'cs':
        return 'C#'
    elif extension == 'lua':
        return 'Lua'
    elif extension == 'hs':
        return 'Haskell'
    elif extension == 'sh':
        return 'Shell'
    elif extension == 'java':
        return 'Java'
    else:
        return 'unkwown'



def  get_extension():
    """Get exension for file that is being edited
    :returns: string
    """
    return vim.eval('expand("%:e")')

